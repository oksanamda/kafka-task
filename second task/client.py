from confluent_kafka import Consumer, Producer, KafkaException
import sys
import json
import getopt


def print_usage_and_exit(program_name):
    sys.stderr.write('Usage: %s [options..] <bootstrap-brokers> <group> <topic1> <topic2> ..\n' % program_name)
    options = '''
 Options:
  -T <intvl>   Enable client statistics at specified interval (ms)
'''
    sys.stderr.write(options)
    sys.exit(1)


if __name__ == '__main__':
    optlist, argv = getopt.getopt(sys.argv[1:], 'T:')
    if len(argv) < 4:
        print_usage_and_exit(sys.argv[0])

    broker = argv[0]
    group = argv[1]
    topic_to = argv[2]
    topic_from = argv[3:]
    conf = {'bootstrap.servers': broker}

    p = Producer(**conf)

    print("Choose command: \n 1. Create car \n 2. Create person \n 3. Put person in car")
    num = 0
    while num < 1 or num > 3:
        num = int(input())

    def delivery_callback(err, msg):
        if err:
            sys.stderr.write('%% Message failed delivery: %s\n' % err)
        else:
            sys.stderr.write('%% Message delivered to %s [%d] @ %d\n' %
                             (msg.topic(), msg.partition(), msg.offset()))

    try:
        p.produce(topic_to, str(num).rstrip(), callback=delivery_callback)

    except BufferError:
        sys.stderr.write('%% Local producer queue is full (%d messages awaiting delivery): try again\n' %
                         len(p))
    p.poll(0)

    sys.stderr.write('%% Waiting for %d deliveries\n' % len(p))
    p.flush()

    if num == 1:
        print("Input car number: ")
        car_info = input()
        info = json.dumps({"car_number": car_info})
    if num == 2:
        print("Input person name: ")
        person_info = input()
        info = json.dumps({"person_name": person_info})
    if num == 3:
        print("Input car number, person name and is person a driver: ")
        car_info = input()
        person_info = input()
        is_driver = bool(input())
        info = json.dumps({"car_number": car_info, "person_name": person_info, "is_driver": is_driver})


    try:
        p.produce(topic_to, info, callback=delivery_callback)

    except BufferError:
        sys.stderr.write('%% Local producer queue is full (%d messages awaiting delivery): try again\n' %
                         len(p))
    p.poll(0)

    sys.stderr.write('%% Waiting for %d deliveries\n' % len(p))
    p.flush()

    conf = {'bootstrap.servers': broker, 'group.id': group, 'session.timeout.ms': 6000,
            'auto.offset.reset': 'earliest'}

    c = Consumer(conf)

    c.subscribe(topic_from)

    try:
        while True:
            msg = c.poll(timeout=1.0)
            if msg is None:
                continue
            if msg.error():
                raise KafkaException(msg.error())
            else:
                sys.stderr.write('%% %s [%d] at offset %d with key %s:\n' %
                                 (msg.topic(), msg.partition(), msg.offset(),
                                  str(msg.key())))
                print(msg.value())

    except KeyboardInterrupt:
        sys.stderr.write('%% Aborted by user\n')

    finally:
        c.close()

