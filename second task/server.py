from confluent_kafka import Consumer, Producer, KafkaException
import sys
import json
import getopt


def print_usage_and_exit(program_name):
    sys.stderr.write('Usage: %s [options..] <bootstrap-brokers> <group> <topic1> <topic2> ..\n' % program_name)
    options = '''
 Options:
  -T <intvl>   Enable client statistics at specified interval (ms)
'''
    sys.stderr.write(options)
    sys.exit(1)


class Car(object):

    def __init__(self, number, seats, driver):
        self.number = number
        self.seats = seats
        self.driver = driver  # есть ли у машины водитель, если есть - его номер


class Person(object):

    def __init__(self, name, car):
        self.name = name
        self.car = car


def search_car_in_list(car_number, car_list):
    for car in car_list:
        if car_number == car.number:
            return True
    return False


def search_person_in_list(person_name, person_list):
    for person in person_list:
        if person_name == person.name:
            return True
    return False


def create_car(info, cars):
    car_num = info.car_info
    # проверить нет ли машины с таким номером
    if not search_car_in_list(car_num, cars):
        number_of_seats = 4
        car = Car(car_num, number_of_seats, None)
        cars.append(car)
        # отправить в кафку, что машина создана
        ans = "Car created successfully."
    else:
        ans = "This car already exist!."
    return ans


def create_person(info, persons):
    pers_name = info.person_info
    # проверить нет ли человека с таким именем
    if not search_person_in_list(pers_name, persons):
        person = Person(pers_name, None)
        persons.append(person)
        ans = "Person created successfully."
    else:
        ans = "Person with this name already exist."

    return ans


def put_person_in_car(info):
    car_number = info.car_info
    person_name = info.person_info
    is_driver = info.is_driver

    if not search_car_in_list(car_number, cars):
        ans = "There is no car with this number"

    for carr in cars:
        if car_number == carr.number:
            car = carr

    if not search_person_in_list(person_name, persons):
        ans = "There is no person with this name"

    for persona in persons:
        if person_name == persona.name:
            person = persona

    if is_driver:
        if car.driver is None:
            # если все норм, то сажаем человека в машину
            person.car = car_number
            car.driver = person_name
            car.seats -= 1
            ans = "You successfully add driver to car!"
        else:
            # если чего-то нет, то отправляет в кафку сообщение о том, чего нет
            ans = "This car already have driver!"
    else:
        if car.seats > 0:
            car.seats -= 1
            ans = "You successfully put person in car!"
        else:
            ans = "There is no car seat for this person"

    return ans


if __name__ == '__main__':
    optlist, argv = getopt.getopt(sys.argv[1:], 'T:')
    if len(argv) < 4:
        print_usage_and_exit(sys.argv[0])

    broker = argv[0]
    group = argv[1]
    topic_from = argv[2]
    topic_to = argv[3:12]

    conf = {'bootstrap.servers': broker, 'group.id': group, 'session.timeout.ms': 6000,
            'auto.offset.reset': 'earliest'}

    def print_assignment(consumer, partitions):
        print('Assignment:', partitions)

    p = Producer(conf)

    def delivery_callback(err, msg):
        if err:
            sys.stderr.write('%% Message failed delivery: %s\n' % err)
        else:
            sys.stderr.write('%% Message delivered to %s [%d] @ %d\n' %
                             (msg.topic(), msg.partition(), msg.offset()))

    c = Consumer(conf)

    c.subscribe(topic_to, on_assign=print_assignment)

    num = 0
    persons = []
    cars = []
    try:
        while True:
            msg = c.poll(timeout=1.0)
            if msg is None:
                continue
            if msg.error():
                raise KafkaException(msg.error())
            else:
                sys.stderr.write('%% %s [%d] at offset %d with key %s:\n' %
                                 (msg.topic(), msg.partition(), msg.offset(),
                                  str(msg.key())))
                num = int(msg.value())
                break
    except KeyboardInterrupt:
        sys.stderr.write('%% Aborted by user\n')

    try:
        while True:
            msg = c.poll(timeout=1.0)
            if msg is None:
                continue
            if msg.error():
                raise KafkaException(msg.error())
            else:
                sys.stderr.write('%% %s [%d] at offset %d with key %s:\n' %
                                 (msg.topic(), msg.partition(), msg.offset(),
                                  str(msg.key())))

                info = json.loads(msg.value().decode('utf18'))

                if num == 1:
                    answer = create_car(info, cars)
                    try:
                        p.produce(topic_from, answer.rstrip(), callback=delivery_callback)

                    except BufferError:
                        sys.stderr.write(
                            '%% Local producer queue is full (%d messages awaiting delivery): try again\n' %
                            len(p))
                    p.poll(0)

                if num == 2:
                    answer = create_person(info, persons)
                    try:
                        p.produce(topic_from, answer.rstrip(), callback=delivery_callback)

                    except BufferError:
                        sys.stderr.write(
                            '%% Local producer queue is full (%d messages awaiting delivery): try again\n' %
                            len(p))
                    p.poll(0)

                if num == 3:
                    answer = put_person_in_car(info)
                    try:
                        p.produce(topic_from, answer.rstrip(), callback=delivery_callback)

                    except BufferError:
                        sys.stderr.write(
                            '%% Local producer queue is full (%d messages awaiting delivery): try again\n' %
                            len(p))
                    p.poll(0)

    except KeyboardInterrupt:
        sys.stderr.write('%% Aborted by user\n')

    finally:
        c.close()
